package ERP.comandas;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

public class ViewOrders {


    public static void main(String[] args) throws SQLException {
        Ventana ventana = new Ventana();

        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

class Ventana extends JFrame {
    protected static String user = "adrian-antanyon-7e3";
    protected static String key = "Contra123";
    protected static String urlDatabase = "jdbc:postgresql://postgresql-adrian-antanyon-7e3.alwaysdata.net/adrian-antanyon-7e3_bbdd";
    //Class.forName("org.postgresql.Driver");

    protected static Connection connection;

    static {
        try {
            connection = DriverManager.getConnection(urlDatabase, user, key);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public Ventana() throws SQLException {

        setLayout(new FlowLayout());


        //Aqui tindre que cargar el combo box amb el resultat de un delec de pais
        JComboBox country = new JComboBox();


        Statement pregunta = connection.createStatement();

        String query = "SELECT * FROM pf_paises";
        ResultSet countryProduct = pregunta.executeQuery(query);
        while (countryProduct.next()){
            String idCountry = countryProduct.getString(1);
            String countryName = countryProduct.getString(2);

            country.addItem(new ComboItem(idCountry, countryName));
        }



        add(country);
        JButton boto = new JButton("El seleccionat");
        boto.addActionListener(actionEvent -> {

            ComboItem item = (ComboItem) country.getSelectedItem();

            System.out.println("El index seleccionat es " + item.getValue() );
        });
        add(boto);

//        Crear las tablas
        DefaultTableModel tableModel = new DefaultTableModel();
//        Crear el panel que contendrá la tabla
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        String productos = "SELECT  pf_productos.nombre as Producto, pf_paises.nombre as Pais FROM pf_paises INNER JOIN pf_productos ON pf_paises.id = pf_productos.id_pais;";
        ResultSet product = pregunta.executeQuery(productos);
        ResultSetMetaData resultSetMetaData =  product.getMetaData();
        while (countryProduct.next()){
            String productName = countryProduct.getString(1);
            String countryName = countryProduct.getString(2);

            System.out.println(productName + " " + countryName);
        }

        int columns = resultSetMetaData.getColumnCount();
        int rows = 0;

        //        Esto lo hago para contar las filas de la tabla
        ResultSet rowsCount = pregunta.executeQuery("SELECT  pf_productos.nombre as Producto, pf_paises.nombre as Pais FROM pf_paises INNER JOIN pf_productos ON pf_paises.id = pf_productos.id_pais;"  );
        while (rowsCount.next()){
            rows++;
        }
//        Aquí consigo el nombre de las columnas
        String[] columnsName = new String[columns];

        for (int index = 1; index <= columns; index++) {
            String columnName = resultSetMetaData.getColumnLabel(index);
            columnsName[index-1] = columnName;
        }

        //        Aquí saco la información que introduciré en la tabla
        String [][] data = new String[rows][columns];

        String masProductos = "SELECT  pf_productos.nombre as Producto, pf_paises.nombre as Pais FROM pf_paises INNER JOIN pf_productos ON pf_paises.id = pf_productos.id_pais;";
        ResultSet otroProduct = pregunta.executeQuery(masProductos);
        int index = 0;
        while (otroProduct.next()) {
            String productName = otroProduct.getString(1);
            String countryName = otroProduct.getString(2);


            data[index][0] = productName;

            data[index][1] = countryName;

            index++;

        }

        JTable myOwnTable = new JTable(data, columnsName);

//        Crear la barra para hacer scroll
        JScrollPane tableView = new JScrollPane(myOwnTable);

        panel.add(tableView);
        add(panel);
        setSize(500, 500);
        setVisible(true);
        setLocationRelativeTo(null);
    }
}

class ComboItem {
    private String value;
    private String label;

    public ComboItem(String value, String label) {
        this.value = value;
        this.label = label;
    }

    public String getValue() {
        return this.value;
    }

    public String getLabel() {
        return this.label;
    }

    @Override
    public String toString() {
        return label;
    }
}


