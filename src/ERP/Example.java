package ERP;

import java.sql.*;
import java.util.Scanner;

public class Example {
    public static void main(String[] args) throws SQLException {
        Scanner lector = new Scanner(System.in);

        String user = "adrian-antanyon-7e3";
        String key = "Contra123";
        String urlDatabase = "jdbc:postgresql://postgresql-adrian-antanyon-7e3.alwaysdata.net/adrian-antanyon-7e3_bbdd";
//        String user = "sjo";
//        String key = "";
//        String urlDatabase = "jdbc:postgresql://127.0.0.1:5432/practica_final";
        //Class.forName("org.postgresql.Driver");

        Connection connection = DriverManager.getConnection(urlDatabase, user, key);


        while (true) {
            int selection = 0;
            try {
                System.out.println("Elige la opción que desees, por favor\n" +
                        "Opcion 1) Listar todos los clientes ordenados por el primer apellido\n" +
                        "Opción 2) Introducir un nuevo cliente \n" +
                        "Opción 3) Eliminar un cliente \n");

                selection = lector.nextInt();

                switch (selection){
                    case 1:
                        Statement pregunta = connection.createStatement();
                        ResultSet resposta = pregunta.executeQuery("SELECT * FROM pf_clientes");

//                        while (resposta.next()) {
//                            int id = resposta.getInt("id");
//                            String nombre = resposta.getString("nombre");
//                            String apellido = resposta.getString("apellido1");
//                            System.out.println("ID: " + id + " - " + nombre + " " + apellido);
//                        }
                        ResultSetMetaData rsmeta = resposta.getMetaData();
                        int columnas = rsmeta.getColumnCount();

                        for (int index = 1; index <= columnas; index++){
                            String nombreColumna = rsmeta.getColumnLabel(index);
                            String tipoColumna = rsmeta.getColumnTypeName(index);
                            System.out.println("La columna " + nombreColumna + " es de tipo " + tipoColumna);
                        }
                        System.out.println();
                        break;
                    case 2:
//                        insertClient(connection);
                        break;
                    case 3:
//                        deleteClient(connection);
                        break;
                    default:
                        System.out.println("Opción no disponible en desarrollo");
                }


            } catch (SQLException ex) {

                System.out.println(ex.getMessage());
                break;
            }

        }
    }
}
