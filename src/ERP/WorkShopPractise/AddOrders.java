package ERP.WorkShopPractise;

import org.postgresql.util.PSQLException;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.sql.*;

public class AddOrders extends JFrame {

    public AddOrders() throws SQLException {
        setTitle("Añadir comanda");
        setBounds(1300, 200, 350, 370);
        JPanel panel = new JPanel();

        panel.setLayout(new GridLayout(7, 1, 1, 5));
        panel.setBorder(new EmptyBorder(15, 15, 15, 15));

        String[] answers = new String[5];

        Statement pregunta = WorkshopPane.connection.createStatement();
        ResultSet resposta = pregunta.executeQuery("SELECT * FROM pf_comandas;");
        ResultSetMetaData rsmeta = resposta.getMetaData();
        int columns = rsmeta.getColumnCount();
        String [] dataFields = new String[5];

        for (int index = 1; index <= columns; index++){
            String columnName = rsmeta.getColumnLabel(index);

            dataFields[index-1] = columnName;
        }

        JTextField idOrder = new JTextField();
        JTextField idClient = new JTextField();
        JTextField idProduct = new JTextField();
        JTextField amount = new JTextField();
        JRadioButton served = new JRadioButton("Entregado");
        JRadioButton unserved = new JRadioButton("Sin entregar");

        for (int index = 0; index < dataFields.length-1; index++) {
            panel.add(createQuestionTextField(dataFields[index]));

            switch (index){
                case 0:
                    panel.add(idOrder);
                    break;
                case 1:
                    panel.add(idClient);
                    break;
                case 2:
                    panel.add(idProduct);
                    break;
                case 3:
                    panel.add(amount);
                    break;
            }
        }

        ButtonGroup radioButtonGroup = new ButtonGroup();
        panel.add(served);
        panel.add(unserved);
        radioButtonGroup.add(served);
        radioButtonGroup.add(unserved);

        JPanel buttonPanel = new JPanel();
        JButton buttonOk = new JButton("OK");

        buttonOk.addActionListener(actionEvent -> {
            int idNewOrder = Integer.parseInt(idOrder.getText());
            int idCurrentClient = Integer.parseInt(idClient.getText());
            int idCurrentProduct = Integer.parseInt(idProduct.getText());
            int amountProductOrder = Integer.parseInt(amount.getText());
            boolean isServed;
            if (served.isSelected()){
                isServed = true;
            }else {
                isServed = false;
            }

            try {
//                AÑADIR DATOS ( C )
                PreparedStatement query = WorkshopPane.connection.prepareStatement("INSERT INTO pf_comandas VALUES(?, ?, ?, ?, ?)");
                query.setInt(1,idNewOrder);
                query.setInt(2, idCurrentClient);
                query.setInt(3,idCurrentProduct);
                query.setInt(4,amountProductOrder);
                query.setBoolean(5,isServed);
                query.executeUpdate();
            } catch (PSQLException error) {
                error.getMessage();
            }catch (SQLException throwables) {
                throwables.printStackTrace();
            }

            dispose();


        });

        JButton buttonCancel = new JButton("Cancel");
        buttonCancel.addActionListener(actionEvent -> dispose());

        buttonPanel.setLayout(new BorderLayout());
        buttonPanel.setBorder(new EmptyBorder(5, 0, 0, 0));
        buttonPanel.add(buttonOk, BorderLayout.WEST);
        buttonPanel.add(buttonCancel, BorderLayout.EAST);
        panel.add(buttonPanel);

        add(panel);
    }

    public JTextField createQuestionTextField(String title){

        if (title.equalsIgnoreCase("")){
            return new JTextField();
        }else {
            Font font = new Font(Font.SANS_SERIF, Font.BOLD, 11);

            JTextField question = new JTextField(title);
            question.setBackground(null);
            question.setFont(font);
            question.setBorder(null);

            return question;
        }


    }

}
