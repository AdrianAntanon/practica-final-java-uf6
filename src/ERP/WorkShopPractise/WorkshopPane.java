package ERP.WorkShopPractise;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.sql.*;

public class WorkshopPane extends JTabbedPane {
    protected static String user = "adrian-antanyon-7e3";
    protected static String key = "Contra123";
    protected static String urlDatabase = "jdbc:postgresql://postgresql-adrian-antanyon-7e3.alwaysdata.net/adrian-antanyon-7e3_bbdd";
    //Class.forName("org.postgresql.Driver");

    protected static Connection connection;

    static {
        try {
            connection = DriverManager.getConnection(urlDatabase, user, key);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public WorkshopPane() throws IOException, SQLException {
        setTabPlacement(JTabbedPane.TOP);

        String routeIMG = "src/ERP/WorkShopPractise/img/";

        ImageIcon seatIcon = new ImageIcon(ImageIO.read(new File(routeIMG+"seat.jpg")).getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        ImageIcon skodaIcon = new ImageIcon(ImageIO.read(new File(routeIMG+"skoda.png")).getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        ImageIcon fiatIcon = new ImageIcon(ImageIO.read(new File(routeIMG+"fiat.png")).getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));

        JPanel pSeat = new JPanel();
        pSeat.setLayout(new GridLayout(1, 1));
        addTab("Clientes", seatIcon,  pSeat);

        JPanel pSkoda = new JPanel();
        pSkoda.setLayout(new GridLayout(1, 1));
        addTab("Productos", skodaIcon, pSkoda);

        JPanel pFiat = new JPanel();
        pFiat.setLayout(new GridLayout(1, 1));
        addTab("Comandas", fiatIcon, pFiat);

        Statement pregunta = connection.createStatement();


        String query = "SELECT id, nombre as Cliente FROM pf_clientes;";
        ResultSet clients = pregunta.executeQuery(query);
        ResultSetMetaData rsmeta = clients.getMetaData();
        int columns = rsmeta.getColumnCount();
        int rows = 0;
        //        Esto lo hago para contar las filas de la tabla y obtener los datos
        while (clients.next()){
            String productName = clients.getString(1);
            String countryName = clients.getString(2);
            rows++;

            System.out.println(productName + " " + countryName);
        }

        //        Aquí consigo el nombre de las columnas
        String[] clientsColumnName = new String[columns];

        for (int index = 1; index <= columns; index++) {
            String columnName = rsmeta.getColumnLabel(index);
            clientsColumnName[index-1] = columnName;
        }

//        Aquí saco la información que introduciré en la tabla
        String [][] clientsTable = new String[rows][columns];
        int clientIndex = 0;

        ResultSet clientsQuery = pregunta.executeQuery(query);
        while (clientsQuery.next()) {
            String id = clientsQuery.getString(1);
            clientsTable[clientIndex][0] = id;
            String clientName = clientsQuery.getString(2);
            clientsTable[clientIndex][1] = clientName;

            clientIndex++;

        }

        String productsQuery = "SELECT id, nombre as Producto FROM pf_productos;";
        ResultSet products = pregunta.executeQuery(productsQuery);
        ResultSetMetaData rsmetaProducts = products.getMetaData();
        int columnsProductsCount = rsmetaProducts.getColumnCount();
        int rowsProductsCount = 0;
        //        Esto lo hago para contar las filas de la tabla y obtener los datos
        while (products.next()){
            rowsProductsCount++;
        }

        System.out.println(columnsProductsCount + " " + rowsProductsCount);

        //        Aquí consigo el nombre de las columnas
        String[] productsColumnName = new String[columnsProductsCount];

        for (int index = 1; index <= columnsProductsCount; index++) {
            String columnName = rsmetaProducts.getColumnLabel(index);
            productsColumnName[index-1] = columnName;
        }

//        Aquí saco la información que introduciré en la tabla
        String [][] productsTable = new String[rowsProductsCount][columnsProductsCount];
        int productsIndex = 0;

        ResultSet newProductsQuery = pregunta.executeQuery(productsQuery);
        while (newProductsQuery.next()) {
            String id = newProductsQuery.getString(1);
            productsTable[productsIndex][0] = id;
            String productName = newProductsQuery.getString(2);
            productsTable[productsIndex][1] = productName;

            productsIndex++;

        }

//        SELECT id, cantidad FROM pf_comandas;

        String ordersQuery = "SELECT id, cantidad FROM pf_comandas;";
        ResultSet orders = pregunta.executeQuery(ordersQuery);
        ResultSetMetaData rsmetaOrders = orders.getMetaData();
        int columnsOrdersCount = rsmetaOrders.getColumnCount();
        int rowsOrdersCount = 0;
        //        Esto lo hago para contar las filas de la tabla y obtener los datos
        while (orders.next()){
            rowsOrdersCount++;
        }

        //        Aquí consigo el nombre de las columnas
        String[] ordersColumnName = new String[columnsOrdersCount];

        for (int index = 1; index <= columnsProductsCount; index++) {
            String columnName = rsmetaOrders.getColumnLabel(index);
            ordersColumnName[index-1] = columnName;
        }

//        Aquí saco la información que introduciré en la tabla
        String [][] ordersTable = new String[rowsOrdersCount][columnsOrdersCount];
        int ordersIndex = 0;

        ResultSet newOrdersQuery = pregunta.executeQuery(ordersQuery);
        while (newOrdersQuery.next()) {
            String id = newOrdersQuery.getString(1);
            ordersTable[ordersIndex][0] = id;
            String orderAmount = newOrdersQuery.getString(2);
            ordersTable[ordersIndex][1] = orderAmount;

            ordersIndex++;

        }

        JTable seatTable = new JTable(clientsTable, clientsColumnName);
        JTable skodaTable = new JTable(productsTable, productsColumnName);
        JTable fiatTable = new JTable(ordersTable, ordersColumnName);


        JScrollPane seatView = new JScrollPane(seatTable);
        JScrollPane skodaView = new JScrollPane(skodaTable);
        JScrollPane fiatView = new JScrollPane(fiatTable);

        pSeat.add(seatView);
        pSkoda.add(skodaView);
        pFiat.add(fiatView);

        JButton addNewOrder = new JButton("Añadir nuevo comanda");
        addNewOrder.addActionListener(e -> {
            AddOrders test = null;

            try {
                test = new AddOrders();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

            test.setVisible(true);
        });
        JPanel panelAddOrder = new JPanel();
        panelAddOrder.add(addNewOrder);
        add(panelAddOrder, "Añadir comanda");

    }
}
