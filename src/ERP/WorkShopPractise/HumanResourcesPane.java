package ERP.WorkShopPractise;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class HumanResourcesPane extends JPanel {
    protected static int rowSelected;
    protected static DefaultTableModel tableModel;
    public HumanResourcesPane(){
        setLayout(new BorderLayout());

        tableModel = new DefaultTableModel();
        JTable myOwnTable = new JTable(tableModel);

        String[] columnName = {"Categoría", "Nombre", "Apellidos","Teléfono","Edad"};

        String [] adri = {"Comercial", "Adrián", "Antañón Orozco", "+34765654432", "26"};
        String [] alex = {"Administrativo", "Alex", "González", "+34765654433", "26"};
        String [] rafa = {"Dirección", "Rafael", "Cuevas", "+34765654416", "36"};
        String [] andrea = {"Administrativo", "Andrea", "Morcillo García", "+347656544326", "25"};
        String [] sergi = {"Contable", "Sergi", "Garriga", "+34747654432", "19"};

        for (String category : columnName) {
            tableModel.addColumn(category);
        }

        tableModel.addRow(adri);
        tableModel.addRow(alex);
        tableModel.addRow(andrea);
        tableModel.addRow(rafa);
        tableModel.addRow(sergi);


        myOwnTable.setPreferredScrollableViewportSize(new Dimension(500, 70));
        myOwnTable.setFillsViewportHeight(true);

        ListSelectionModel listSelectionModel = myOwnTable.getSelectionModel();
        listSelectionModel.addListSelectionListener(e -> {
            rowSelected = myOwnTable.getSelectedRow();
        });

        JButton addNewHR = new JButton("Añadir nuevo personal");
        addNewHR.addActionListener(e -> {
            AddPersonal test = new AddPersonal();
            test.setVisible(true);
        });
        JPanel panelAddNewHR = new JPanel();
        panelAddNewHR.add(addNewHR);
        add(panelAddNewHR, BorderLayout.SOUTH);

        JScrollPane tableView = new JScrollPane(myOwnTable);
        add(tableView);
    }
}
