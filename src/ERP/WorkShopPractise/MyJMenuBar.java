package ERP.WorkShopPractise;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class MyJMenuBar extends JMenuBar {
    public MyJMenuBar() {

        class MenuListenerExample implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                JMenuItem selection = (JMenuItem) e.getSource();
                String action = selection.getActionCommand();
                if (action.compareTo("About")==0){
                    JOptionPane.showMessageDialog(
                            MyJMenuBar.this,
                            "ITB ERP",
                            "Información",
                            JOptionPane.INFORMATION_MESSAGE
                    );

                }else if (action.compareTo("Salir")==0){
                    int result = JOptionPane.showOptionDialog(
                            getRootPane(),
                            "¿Estás seguro que quieres salir?",
                            "Salir",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.WARNING_MESSAGE,
                            null,
                            new Object[]{"Sí", "No"},
                            "Sí");
                    if (result == 0){
                        MainERP.myERP.dispose();
                    }
                }else if (action.compareTo("Nuevo")==0){
                    AddPersonal test = new AddPersonal();
                    test.setVisible(true);
                }
            }
        }

        MenuListenerExample menuListener = new MenuListenerExample();

        JMenu file = new JMenu("Archivo");
        JMenuItem newFile = new JMenuItem("Nuevo");
        newFile.addActionListener(menuListener);
        file.add(newFile);

        JMenuItem open = new JMenuItem("Abrir");
        open.addActionListener(menuListener);
        file.add(open);

        JMenuItem exit = new JMenuItem("Salir");
        exit.addActionListener(menuListener);
        file.add(exit);

        add(file);

        JMenu help = new JMenu("Ayuda");
        JMenuItem about = new JMenuItem("About");
        about.addActionListener(menuListener);
        help.add(about);

        add(help);
    }
}
