package ERP.WorkShopPractise;

import java.io.IOException;
import java.sql.SQLException;

public class MainERP {
    protected static MyJFrame myERP;

    public static void main(String[] args) throws IOException, SQLException {
        myERP = new MyJFrame("Mi taller");

        myERP.setVisible(true);
        myERP.setDefaultCloseOperation(MyJFrame.EXIT_ON_CLOSE);
    }
}
