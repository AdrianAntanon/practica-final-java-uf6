package ERP.WorkShopPractise;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

class AddOrDeleteToolBar extends JToolBar {
    static JButton addPersonal;
    static JButton deletePersonal;

    public AddOrDeleteToolBar() throws IOException {
        String ROUTE_IMG = "src/ERP/WorkShopPractise/img/";
        String[] imgCategory = {"add", "delete"};
        String extensionFile = ".png";
        ImageIcon img = new ImageIcon(ImageIO.read(new File(ROUTE_IMG + imgCategory[0]+ extensionFile)).getScaledInstance(50, 50, Image.SCALE_SMOOTH));

        AddPersonal newEmployee = new AddPersonal();
        newEmployee.setVisible(false);

        addPersonal = new JButton("Añadir", img);
        addPersonal.addActionListener(actionEvent -> newEmployee.setVisible(true));
        addPersonal.setEnabled(false);
        add(addPersonal);

        img = new ImageIcon(ImageIO.read(new File(ROUTE_IMG + imgCategory[1]+ extensionFile)).getScaledInstance(50, 50, Image.SCALE_SMOOTH));

        deletePersonal = new JButton("Eliminar", img);
        deletePersonal.addActionListener(actionEvent -> HumanResourcesPane.tableModel.removeRow(HumanResourcesPane.rowSelected));
        deletePersonal.setEnabled(false);
        add(deletePersonal);
    }
}
