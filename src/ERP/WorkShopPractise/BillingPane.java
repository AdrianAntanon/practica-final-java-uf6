package ERP.WorkShopPractise;

import javax.swing.*;
import java.awt.*;
import java.sql.*;

public class BillingPane extends JPanel {
    protected static String user = "adrian-antanyon-7e3";
    protected static String key = "Contra123";
    protected static String urlDatabase = "jdbc:postgresql://postgresql-adrian-antanyon-7e3.alwaysdata.net/adrian-antanyon-7e3_bbdd";
    //Class.forName("org.postgresql.Driver");

    protected static Connection connection;

    static {
        try {
            connection = DriverManager.getConnection(urlDatabase, user, key);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public BillingPane() throws SQLException {
        setLayout(new BorderLayout());

        Statement pregunta = connection.createStatement();

        ResultSet resposta = pregunta.executeQuery("SELECT * FROM pf_paises");
        ResultSetMetaData rsmeta = resposta.getMetaData();
        int columns = rsmeta.getColumnCount();
        int rows = 0;

        String query = "SELECT  pf_productos.nombre, pf_paises.nombre FROM pf_paises INNER JOIN pf_productos ON pf_paises.id = pf_productos.id_pais;";
        ResultSet countryProduct = pregunta.executeQuery(query);
        ResultSetMetaData resultSetMetaData =  countryProduct.getMetaData();
        while (countryProduct.next()){
            String productName = countryProduct.getString(1);
            String countryName = countryProduct.getString(2);

            System.out.println(productName + " " + countryName);
        }


//        Esto lo hago para contar las filas de la tabla
        ResultSet rowsCount = pregunta.executeQuery("SELECT * FROM pf_paises");
        while (rowsCount.next()){
            rows++;
        }

//        Aquí consigo el nombre de las columnas
        String[] columnsName = new String[columns];

        for (int index = 1; index <= columns; index++) {
            String columnName = rsmeta.getColumnLabel(index);
            columnsName[index-1] = columnName;
        }


//        Aquí saco la información que introduciré en la tabla
        String [][] data = new String[rows][columns];
        int index = 0;
        while (resposta.next()) {
            int id = resposta.getInt("id");
            String idString = id + "";

            data[index][0] = idString;

            String name = resposta.getString("nombre");

            data[index][1] = name;

            index++;

        }

        JTable myOwnTable = new JTable(data, columnsName);

        myOwnTable.setPreferredScrollableViewportSize(new Dimension(500, 70));
        myOwnTable.setFillsViewportHeight(true);

        ListSelectionModel listSelectionModel = myOwnTable.getSelectionModel();
        listSelectionModel.addListSelectionListener(e -> {
            int row = myOwnTable.getSelectedRow();
            int column = myOwnTable.getSelectedColumn();
            String selectedItem = (String) myOwnTable.getValueAt(row, column);
            JOptionPane.showMessageDialog(null, "Se ha seleccionado fila: " + (row+1) + " y columna: " + (column+1) +
                    "\n El valor de la celda es: " + selectedItem);
        });

        JScrollPane tableView = new JScrollPane(myOwnTable);
        add(tableView);

    }
}
