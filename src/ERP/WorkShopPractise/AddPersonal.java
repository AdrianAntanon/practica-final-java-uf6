package ERP.WorkShopPractise;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

class AddPersonal extends JFrame {
    
    public AddPersonal() {
        setTitle("Test");
        setBounds(1300, 200, 250, 370);
        JPanel panel = new JPanel();

        panel.setLayout(new GridLayout(10, 1, 1, 5));
        panel.setBorder(new EmptyBorder(15, 15, 15, 15));

        String[] answers = new String[5];

        String[] categories = {"Seleccione una opción: ", "Comercial", "Administrativo", "Mecánico", "Dirección"};
        JComboBox comboBox = new JComboBox(categories);
        comboBox.addItemListener(itemEvent -> {
            answers[0] = (String) itemEvent.getItem();
            if (answers[0].equals("Seleccione una opción: ")) {
                answers[0]="";
            }
        });
        comboBox.addItemListener(itemEvent -> answers[0] = (String) itemEvent.getItem());

        panel.add(comboBox);

        String [] fields = {"Nombre: ","Apellidos: ", "Teléfono: ", "Edad: "};

        JTextField userName = new JTextField();
        JTextField userSurname = new JTextField();
        JTextField userMobile = new JTextField();
        JTextField userAge = new JTextField();

        for (int index = 0; index < fields.length; index++) {
            panel.add(createQuestionTextField(fields[index]));

            switch (index){
                case 0:
                    panel.add(userName);
                    break;
                case 1:
                    panel.add(userSurname);
                    break;
                case 2:
                    panel.add(userMobile);
                    break;
                case 3:
                    panel.add(userAge);
                    break;
            }
        }

        JPanel buttonPanel = new JPanel();
        JButton buttonOk = new JButton("OK");
        buttonOk.addActionListener(actionEvent -> {
            answers[1] = userName.getText();
            answers[2] = userSurname.getText();
            answers[3] = userMobile.getText();
            answers[4] = userAge.getText();

            HumanResourcesPane.tableModel.addRow(answers);
        });
        JButton buttonCancel = new JButton("Cancel");
        buttonCancel.addActionListener(actionEvent -> dispose());


        buttonPanel.setLayout(new GridLayout(1, 4, 10, 1));
        buttonPanel.setBorder(new EmptyBorder(5, 0, 0, 0));
        buttonPanel.add(buttonOk);
        buttonPanel.add(buttonCancel);

        panel.add(buttonPanel);

        add(panel);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }


    public JTextField createQuestionTextField(String title){

        if (title.equalsIgnoreCase("")){
            return new JTextField();
        }else {
            Font font = new Font(Font.SANS_SERIF, Font.BOLD, 11);

            JTextField question = new JTextField(title);
            question.setBackground(null);
            question.setFont(font);
            question.setBorder(null);

            return question;
        }


    }

}
